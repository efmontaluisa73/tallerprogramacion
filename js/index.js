$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 1000
    });

    $("#contacto").on("show.bs.modal", function (e) {
        console.log("El modal se está mostrando");
        $(e.relatedTarget).removeClass("btn-outline-success");
        $(e.relatedTarget).addClass("btn-primary");
        $(e.relatedTarget).prop("disabled", true);
    });
    $("#contacto").on("shown.bs.modal", function (e) {
        console.log("El modal se mostró");
    });
    $("#contacto").on("hide.bs.modal", function (e) {
        console.log("El modal se oculta");
    });
    $("#contacto").on("hidden.bs.modal", function (e) {
        console.log("El modal se ocultó");
        //Reiniciar Boton 0
        $("#contactoBtn0").removeClass("btn-primary");
        $("#contactoBtn0").addClass("btn-outline-success");
        $("#contactoBtn0").prop("disabled", false);
        //Reiniciar Boton 1
        $("#contactoBtn1").removeClass("btn-primary");
        $("#contactoBtn1").addClass("btn-outline-success");
        $("#contactoBtn1").prop("disabled", false);
        //Reiniciar Boton 2
        $("#contactoBtn2").removeClass("btn-primary");
        $("#contactoBtn2").addClass("btn-outline-success");
        $("#contactoBtn2").prop("disabled", false);
        //Reiniciar Boton 3
        $("#contactoBtn3").removeClass("btn-primary");
        $("#contactoBtn3").addClass("btn-outline-success");
        $("#contactoBtn3").prop("disabled", false);
        //Reiniciar Boton 4
        $("#contactoBtn4").removeClass("btn-primary");
        $("#contactoBtn4").addClass("btn-outline-success");
        $("#contactoBtn4").prop("disabled", false);
    });
});